words = set([])

rem = set(["you","your","me","she","he","is","am","are","was","were","him","his","her","when","where","how","what","whose","which","why","this","that","the","them","their","these","those"])

list_words=[]
def read_words(file_name):
	f = open(file_name,"r")
	lines = f.readlines()
	for line in lines:
		for word in line.split()[1:]:
			if word[0] == '@':
				rem.add(word[1:])
			if word[0] == '#':
				rem.add(word[1:])
			list_words.append(word.lower())

read_words("TweetsCorpus/twitter_negative")
read_words("TweetsCorpus/twitter_objective")
read_words("TweetsCorpus/twitter_positive")

print "Init:" ,len(list_words)

list_words=sorted(list_words)
for x in xrange(len(list_words)-1):
	if list_words[x] == list_words[x+1]:
		words.add(list_words[x])

import re
for w in rem:
	words.add(w)

for word in words:
	if "http" in word:
		rem.add(word)
	if len(re.findall('[-0-9/$#&@.\'",!?*]',word))>0:
		rem.add(word)
	if len(word) < 3:
		rem.add(word)

for w in rem:
	words.remove(w)

print "Final:" ,len(words)

# print words

f = open("input.data","w")
f1 = open("check.data","w")
data=str(len(words)) + " 2\n"
count = 1;
f.write(str(len(words)) + " 2\n")
def gen_line(line,words,output):
	global f,f1,count
	if count%5 != 0 :
		for w in words:
			if w in line:
				f.write("1 ")
			else:
				f.write("0 ")
		if output==0:
			f.write("0 0")
		elif output==1:
			f.write("1 0")
		else :
			f.write("1 1")
		f.write("\n")
		count += 1
	else :
		for w in words:
			if w in line:
				f1.write("1 ")
			else:
				f1.write("0 ")
		if output==0:
			f1.write("0 0")
		elif output==1:
			f1.write("1 0")
		else :
			f1.write("1 1")
		f1.write("\n")
		count = 1

def get_array(file_name,output):
	if output==0:
		print "Reading twitter_negative"
	elif output==1:
		print "Reading twitter_objective"
	else :
		print "Reading twitter_positive"
	f = open(file_name,"r")
	lines = f.readlines()
	for line in lines:
		gen_line(line,words,output)
	f.close()

get_array("TweetsCorpus/twitter_negative",0)
get_array("TweetsCorpus/twitter_objective",1)
get_array("TweetsCorpus/twitter_positive",2)
print "Writing input.data"

f.close()

data=""
for w in words:
	data+=w
	data+="\n"

f = open("words.data","w")
print "Writing words.data"
f.write(data)
f.close()
