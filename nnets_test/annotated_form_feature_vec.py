words = set([])

rem = set(["you","your","me","she","he","is","am","are","was","were","him","his","her","when","where","how","what","whose","which","why","this","that","the","them","their","these","those"])

ans_vecs=[]

list_words=[]
def read_words(file_name):
	f = open(file_name,"r")
	lines = f.readlines()
	for line in lines:
		if line.find("$$pos$$")!=-1:
			ans_vecs.append(" 1 1")
		elif line.find("$$neg$$")!=-1:
			ans_vecs.append(" 0 0")
		else:
			ans_vecs.append(" 1 0")
		# print(ans_vecs[len(ans_vecs)-1])
		for word in line.split():
			if word[0] == '@':
				rem.add(word[1:])
			if word[0] == '#':
				list_words.append((word[1:]).lower())
			else:
				list_words.append(word.lower())

read_words("TweetsCorpus/combinedCorpusFinal")

list_words=sorted(list_words)
for x in xrange(len(list_words)-1):
	if list_words[x] == list_words[x+1]:
		words.add(list_words[x])

print "Init:" ,len(words)

import re
for w in rem:
	words.add(w)

for word in words:
	if "http" in word:
		rem.add(word)
	if len(re.findall('[-0-9/$#&@.\'",!?*]',word))>0:
		rem.add(word)
	if len(word) < 3:
		rem.add(word)

for w in rem:
	words.remove(w)

print "Final:" ,len(words)

# print words

f = open("input.data","w")
f1 = open("check.data","w")
data=str(len(words)) + " 2\n"
count = 0;
f.write(str(len(words)) + " 2\n")
def gen_line(line,words):
	global f,f1,count
	if count%5 != 0 :
		for w in words:
			if w in line:
				f.write("1 ")
			else:
				f.write("0 ")
		f.write(ans_vecs[count])
		f.write("\n")
		count += 1
	else :
		for w in words:
			if w in line:
				f1.write("1 ")
			else:
				f1.write("0 ")
		f1.write(ans_vecs[count])
		f1.write("\n")
		count += 1

def get_array(file_name):
	f = open(file_name,"r")
	lines = f.readlines()
	for line in lines:
		gen_line(line,words)
	f.close()

get_array("TweetsCorpus/combinedCorpusFinal")
print "Writing input.data"

f.close()

data=""
for w in words:
	data+=w
	data+="\n"

f = open("words.data","w")
print "Writing words.data"
f.write(data)
f.close()
