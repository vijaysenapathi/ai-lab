#include <iostream>
#include <vector>
using namespace std;

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;


bool dot_prod(vi &a,vi &b){
  int sum=0;
  for (int i = 0; i < a.size(); i++)
  {
    sum+=a[i]*b[i];
  }
  return sum>0;
}

vi sum_vec(vi &a,vi &b){
  vi c;
  for (int i = 0; i < a.size(); i++)
  {
    c.push_back(a[i]+b[i]);
  }
  return c;
}

int main(){
  int n;
  cin >> n;
  ll num = 1<<n;
  vvi preprocessed;
  for (int i = 0; i < num; i++)
  {
    vi vec;
    int val;
    for (int j = 0; j < n; j++)
    {
      cin >> val;
      vec.push_back(val);
    }
    vi temp;
    int mul = 1;
    cin >> val;
    if (val == 0) mul = -1;
    for (int j = 0; j < n; j++)
    {
      temp.push_back(vec[j]*mul);
    }
    temp.push_back(-1*mul);
    preprocessed.push_back(temp);
  }
  //preprocessed
  int cur = 0 ;
  vi w;
  for (int i = 0; i <= n; i++)
  {
    w.push_back(0);
  }
  ll count = 0;
  
  while(true){
    //count++;if (count>10000) break;
    cout << cur << " ";
    for (int i = 0; i < n+1; i++)
    {
      cout << w[i] << " ";
    }
    cout << endl;
    if (cur==num) break;
    if (dot_prod(w,preprocessed[cur])) cur++;
    else {
      w = sum_vec(w,preprocessed[cur]);
      cur = 0;
    }
  }
  for (int i = 0; i < n; i++)
  {
    cout << "w" << i << " " << w[i] << endl ;
  }
  cout << "Theta" << " " << w[n] << endl;
  
  return 0;
}

