#include <iostream>
using namespace std;

void print_gc(int n,string prev){
  if (n==0) {
    string sp="";
    for (int i = 0; i < prev.size(); i++)
    {
      if(prev[i]!=' ') sp += prev[i];
    }
    string palin = " 1";
    int zeroc=0,onec=0;
    for (int i = 0; i < sp.size(); i++)
    {
      if (sp[i]=='0') zeroc++;
      else onec++;
    }
    if (onec%2==0) palin = " 1";
    else palin = " 0";
    cout << prev+palin << endl;
  }
  else {
    print_gc(n-1,prev + " 1");
    print_gc(n-1,prev + " 0");
  }
}

int main(){
  print_gc(6,"");
}
