set terminal png
set output "better_heuristic.png"
set title "Hamming-Manhattan nodes v/s Depth"
set ylabel "No. of Nodes expanded"
set xlabel "Depth"
set datafile separator ' ' 
set logscale y
plot "out" using 1:2 smooth unique title "Hamming","out" using 1:3 smooth unique title "Manhattan"
