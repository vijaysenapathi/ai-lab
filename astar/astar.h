#ifndef ASTAR_H
#define ASTAR_H

#include <vector>
#include <queue>
#include <iostream>
using namespace std;

template <class state>
class astar{
public:
	astar(){

	}
	astar(const astar& a){
		startState = a.startState;
		endState = a.endState;
		openlist = a.openlist;
		closedlist = a.closedlist;
	}
	~astar(){

	}

	vector<state> nextstate(state state1){
		return state1.nextstate();
	}

	double heuristic(state state1){
		return state1.heuristic();
	}

	void setInitialState(state state1){
		startState = state1;
	}

	void setFinalState(state state1){
		endState = state1;
	}

	void solve(){
		//add startstate to open queue
		startState.bestDistance = 0;
		startState.bestParent = -1;
		openlist.push(startState);
		while (!openlist.empty()){
			state s = openlist.top();
			openlist.pop();
			int bestDistance = s.bestDistance;
			int heuristicValue = (int)s.heuristic();
			closedlist.push_back(s);

			if (s == endState){
				//handle parent pointer redirection and print output
				state tempState;
				tempState = s;
				tempState.print();
				int dist=0;
				while(tempState.bestParent != -1){
					dist++;
					tempState = closedlist[tempState.bestParent];
					tempState.print();
				}
				cout << "Distance = " << dist << endl;
                cout << "The size of closed list is "<< closedlist.size()<<endl;
				return;
			}
			else{
				vector<state> nextStates = nextstate(s);
				for (unsigned int i = 0; i < nextStates.size(); i++){
					bool present = false;
					unsigned int j;
					//checking whether the state is already explored
					for (j = 0; j < closedlist.size(); j++){
						if (closedlist[j] == nextStates[i]){
							present = true;
							break;
						}
					}
					if (present){
						//update if necessary
						int newDistance = bestDistance + 1;
						if (newDistance < closedlist[j].bestDistance){
							cout << "Updating parent pointers" << endl;

							closedlist[j].bestDistance = newDistance;
							closedlist[j].bestParent = closedlist.size() - 1;
						}
					}
					else{
						//updating distance before adding them to the openlist
						nextStates[i].bestParent = closedlist.size() - 1;
						nextStates[i].bestDistance = closedlist[closedlist.size() - 1].bestDistance + 1;

						//add this state to the openlist
						openlist.push(nextStates[i]);
					}
				}
				// delete &nextStates [];
			}
		}
	}
	
	

	priority_queue<state> openlist;
	vector<state> closedlist;
	state startState,endState;

private:


};

#endif
