#include <iostream>
#include <vector>
#include <queue>

#include "cannibal.h"
#include "astar.h"

using namespace std;



int main(){
	astar<statePCM> star;
	statePCM temp;
	


	temp.set(0,0,true);
	star.setInitialState(temp);
	
	temp.set(3,3,false);
	star.setFinalState(temp);
	
	star.solve();

    return 0;

}
