#include <iostream>
#include <vector>
using namespace std;

class node{
public:
	node(){
	}
	node(int id1){
		id = id1;
		h = 0;
		g = 0;
		f = 0;
	}
	node(const node& n){
		id = n.id;
	}
	~node(){

	}

	int id;
	int h;
	int g;
	int f;
	vector<node*> connected;
};

class link{
public:
	link(){

	}
	link(double wt, node *nd1, node *nd2){
		weight = wt;
		node1 = nd1;
		node2 = nd2;
	}
	link(const link& l){

	}
	~link(){

	}
	node *node1, *node2;
	double weight;
};

class graph{
public:
	graph(){

	}
	graph(const graph&){
		//blabla
	}
	~graph(){

	}

	void addNode(int id){
		node new_node;
		nodes.push_back(new_node);
	}
	node* getNodePtr(int id1){
		for (int i = 0; i < nodes.size(); i++){
			if (nodes[i].id == id1){
				return &nodes[i];
			}
		}
		return NULL;
	}
	void connectNode(int id1, int id2, double wt){
		node *node1ptr = NULL, *node2ptr = NULL;
		for (int i = 0; i < nodes.size(); i++){
			if (nodes[i].id == id1){
				node1ptr = &nodes[i];
			}
			if (nodes[i].id == id2){
				node2ptr = &nodes[i];
			}
		}
		if (node1ptr == NULL || node2ptr == NULL){
			//do nothing
			return;
		}
		else{
			(*node1ptr).connected.push_back(node2ptr);
			(*node2ptr).connected.push_back(node1ptr);
			link new_link(wt, node1ptr, node2ptr);
			links.push_back(new_link);
		}
	}
	vector<node> nodes;
	vector<link> links;
};

