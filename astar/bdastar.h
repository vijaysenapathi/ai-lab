#ifndef BDbdastar_H
#define BDbdastar_H

#include <vector>
#include <queue>
#include <map>
#include <string>
using namespace std;

template <class state>
class bdastar{
public:
	bdastar(){

	}
	bdastar(const bdastar& a){
		startState = a.startState;
		endState = a.endState;
		openlistStart = a.openlistStart;
		closedlistStart = a.closedlistStart;
		openlistEnd = a.openlistEnd;
		closedlistEnd = a.closedlistEnd;
        visitedNodes = a.visitedNodes;
	}
	~bdastar(){

	}

	vector<state> nextstate(state state1){
		return state1.nextstate();
	}

	double heuristic(state state1){
		return state1.heuristic();
	}

	void setInitialState(state state1){
		startState = state1;
	}

	void setFinalState(state state1){
		endState = state1;
	}

	void solve(){
		//add startstate to open queue
		startState.bestDistance = 0;
		startState.bestParent = -1;
		openlistStart.push(startState);

		endState.bestDistance = 0;
		endState.bestParent = -1;
		openlistEnd.push(endState);

		while (!openlistEnd.empty() && !openlistStart.empty()){
			state start = nextStateStart();
			state end = nextStateEnd();

            //test for common nodes
            if(visitedNodes.count(start.getUniqueString()) > 0){
                cout<<"Common node found equal to node expanded from start"<<endl;

                int dist = 0;

                cout<<"Trace from start"<<endl;

                state tempState;
                tempState = start;
                tempState.print();
                while(tempState.bestParent != -1){
                    dist++;
                    tempState = closedlistStart[tempState.bestParent];
                    tempState.print();
                }

                cout<<"Trace from end"<<endl;

				tempState = closedlistEnd[visitedNodes[start.getUniqueString()]];
                tempState.print();
                while(tempState.bestParent != -1){
                    dist++;
                    tempState = closedlistEnd[tempState.bestParent];
                    tempState.print();
                }

				dist++;

                cout<<"Distance found is "<<dist<<endl;
                cout<<"Total nodes in closed lists: "<<closedlistStart.size() + closedlistEnd.size()<<endl;

                break;
			}
			else if (visitedNodes.count(end.getUniqueString()) > 0){
                cout<<"Common node found equal to node expanded from end"<<endl;


                int dist = 0;

                cout<<"Trace from start"<<endl;

                state tempState;

				tempState = closedlistStart[visitedNodes[end.getUniqueString()]];
 
                tempState.print();
                while(tempState.bestParent != -1){
                    dist++;
                    tempState = closedlistStart[tempState.bestParent];
                    tempState.print();
                }

                cout<<"Trace from end"<<endl;

                tempState = end;
                tempState.print();
                while(tempState.bestParent != -1){
                    dist++;
                    tempState = closedlistEnd[tempState.bestParent];
                    tempState.print();
                }

				dist++;

                cout<<"Distance found is "<<dist<<endl;

                cout<<"Total nodes in closed lists: "<<closedlistStart.size() + closedlistEnd.size()<<endl;


                break;
            }else if(start == end){
                cout<<"etart and end are the same node"<<endl;
                break;
            }else{
				solveStart(start);
				solveEnd(end);
			}
		}
        if(openlistEnd.empty() || openlistStart.empty()){
            cout<<"Open list became empty"<<endl;
        }
	}

	state nextStateStart(){
		state s = openlistStart.top();
		openlistStart.pop();
		return s;
	}

	state nextStateEnd(){
		state s = openlistEnd.top();
		openlistEnd.pop();
		return s;
	}

	void solveStart(state s){
		if (visitedNodes.count(s.getUniqueString()) > 0){
			cout << "came here" << endl;
			return;
		}
		int bestDistance = s.bestDistance;
		int heuristicValue = (int)s.heuristic(endState);
		closedlistStart.push_back(s);


        //add this state to map
        visitedNodes[ s.getUniqueString() ] = closedlistStart.size() - 1; 

		vector<state> nextStates = nextstate(s);
		for (unsigned int i = 0; i < nextStates.size(); i++){
			bool present = false;
			unsigned int j;
			//checking whether the state is already explored
			for (j = 0; j < closedlistStart.size(); j++){
				if (closedlistStart[j] == nextStates[i]){
					present = true;
					break;
				}
			}
			if (present){
				//update if necessary
				int newDistance = bestDistance + 1;
				if (newDistance < closedlistStart[j].bestDistance){
					closedlistStart[j].bestDistance = newDistance;
					closedlistStart[j].bestParent = closedlistStart.size() - 1;
				}
			}
			else{
				//updating distance before adding them to the openlist
				nextStates[i].bestParent = closedlistStart.size() - 1;
				nextStates[i].bestDistance = closedlistStart[closedlistStart.size() - 1].bestDistance + 1;

				//add 
                //his state to the openlist
				openlistStart.push(nextStates[i]);

			}
		}
	}
	
	void solveEnd(state s){
		if (visitedNodes.count(s.getUniqueString()) > 0){
			cout << "came here" << endl;
			return;
		}

		int bestDistance = s.bestDistance;
		int heuristicValue = (int)s.heuristic(startState);
		closedlistEnd.push_back(s);

		//add this state to map
		visitedNodes[s.getUniqueString()] = closedlistEnd.size() - 1;


		vector<state> nextStates = nextstate(s);
		for (unsigned int i = 0; i < nextStates.size(); i++){
			bool present = false;
			unsigned int j;
			//checking whether the state is already explored
			for (j = 0; j < closedlistEnd.size(); j++){
				if (closedlistEnd[j] == nextStates[i]){
					present = true;
					break;
				}
			}
			if (present){
				//update if necessary
				int newDistance = bestDistance + 1;
				if (newDistance < closedlistEnd[j].bestDistance){
					closedlistEnd[j].bestDistance = newDistance;
					closedlistEnd[j].bestParent = closedlistEnd.size() - 1;
				}
			}
			else{
				//updating distance before adding them to the openlist
				nextStates[i].bestParent = closedlistEnd.size() - 1;
				nextStates[i].bestDistance = closedlistEnd[closedlistEnd.size() - 1].bestDistance + 1;

				//add this state to the openlist
				openlistEnd.push(nextStates[i]);

			}
		}
	}

	priority_queue<state> openlistStart;
	vector<state> closedlistStart;

	priority_queue<state> openlistEnd;
	vector<state> closedlistEnd;

	state startState,endState;

    map<string,int> visitedNodes;//unique key for a state, position in its corresponding closed list

private:


};

#endif
