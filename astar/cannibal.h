#ifndef STATEPCM_H
#define STATEPCM_H

#include "astar.h"
#include <iostream>
#include <cmath>
using namespace std;

class statePCM{
    public:
        statePCM(){
            bestDistance = -1;
            bestParent = -1;
            heuisticValue = -1;
            source = true;
        }
        statePCM(const statePCM& t){
            missionaries = t. missionaries;
            cannibals = t.cannibals;
            bestParent = t.bestParent;
            bestDistance = t.bestDistance;
            heuisticValue = t.heuisticValue;
            source =t.source;
        }
        ~statePCM(){

        }
        void set(int m,int c,bool boat){
            missionaries = m;
            cannibals = c;
            source = boat;
        }
        bool operator==(const statePCM& s){
            if (this->missionaries == s.missionaries && this->cannibals == s.cannibals && (source == s.source)){
                return true;
            }
            else{
                return false;
            }
        }

        void print(){
            if(source){
                cout<<"m:"<<3-missionaries<<", c:"<<3-cannibals<<"[]___m:"<<missionaries<<", c:"<<cannibals<<endl;
            }else{
                cout<<"m:"<<3-missionaries<<", c:"<<3-cannibals<<"___[]m:"<<missionaries<<", c:"<<cannibals<<endl;

            }
        }

        vector<statePCM> &nextstate(){
            statePCM newpuzzle;
            vector<statePCM> *result = new vector<statePCM>;

            int M,C,m,c;
            M = this->missionaries;
            C = this->cannibals;

            //only 1 in the boat
            m = M-1;
            c = C;
			if (0 <= m && m <= 3 && 0 <= c && c <= 3 && ((m >= c && (3 - m) >= (3 - c)) || (m >= c && m == 3) || ((3 - m) >= (3 - c) && m == 0)) && !source){
                newpuzzle.set(m,c,true);
                result->push_back(newpuzzle);
            }

            m = M;
            c = C-1;
			if (0 <= m && m <= 3 && 0 <= c && c <= 3 && ((m >= c && (3 - m) >= (3 - c)) || (m >= c && m == 3) || ((3 - m) >= (3 - c) && m == 0)) && !source){
                newpuzzle.set(m,c,true);
                result->push_back(newpuzzle);
            }

            m = M+1;
            c = C;
			if (0 <= m && m <= 3 && 0 <= c && c <= 3 && ((m >= c && (3 - m) >= (3 - c)) || (m >= c && m == 3) || ((3 - m) >= (3 - c) && m == 0)) && source){
                newpuzzle.set(m,c,false);
                result->push_back(newpuzzle);
            }

            m = M;
            c = C+1;
			if (0 <= m && m <= 3 && 0 <= c && c <= 3 && ((m >= c && (3 - m) >= (3 - c)) || (m >= c && m == 3) || ((3 - m) >= (3 - c) && m == 0)) && source){
                newpuzzle.set(m,c,false);
                result->push_back(newpuzzle);
            }
            //two in a boat
            m = M-2;
            c = C;
			if (0 <= m && m <= 3 && 0 <= c && c <= 3 && ((m >= c && (3 - m) >= (3 - c)) || (m >= c && m == 3) || ((3 - m) >= (3 - c) && m == 0)) && !source){
                newpuzzle.set(m,c,true);
                result->push_back(newpuzzle);
            }

            m = M;
            c = C-2;
			if (0 <= m && m <= 3 && 0 <= c && c <= 3 && ((m >= c && (3 - m) >= (3 - c)) || (m >= c && m == 3) || ((3 - m) >= (3 - c) && m == 0)) && !source){
                newpuzzle.set(m,c,true);
                result->push_back(newpuzzle);
            }

            m = M+2;
            c = C;
			if (0 <= m && m <= 3 && 0 <= c && c <= 3 && ((m >= c && (3 - m) >= (3 - c)) || (m >= c && m == 3) || ((3 - m) >= (3 - c) && m == 0)) && source){
                newpuzzle.set(m,c,false);
                result->push_back(newpuzzle);
            }

            m = M;
            c = C+2;
			if (0 <= m && m <= 3 && 0 <= c && c <= 3 && ((m >= c && (3 - m) >= (3 - c)) || (m >= c && m == 3) || ((3 - m) >= (3 - c) && m == 0)) && source){
                newpuzzle.set(m,c,false);
                result->push_back(newpuzzle);
            }

            m = M-1;
            c = C-1;
			if (0 <= m && m <= 3 && 0 <= c && c <= 3 && ((m >= c && (3 - m) >= (3 - c)) || (m >= c && m == 3) || ((3 - m) >= (3 - c) && m == 0)) && !source){
                newpuzzle.set(m,c,true);
                result->push_back(newpuzzle);
            }

            m = M+1;
            c = C+1;
			if (0 <= m && m <= 3 && 0 <= c && c <= 3 && ((m >= c && (3 - m) >= (3 - c)) || (m >= c && m == 3) || ((3 - m) >= (3 - c) && m == 0)) && source){
                newpuzzle.set(m,c,false);
                result->push_back(newpuzzle);
            }
            return *result;
        }
        double heuristic() const{
            //return 0;//no heuristic
			//return heuristic1();//number of displaced guys from final state divided by 2 
			return heuristic2();//number of displaced guys from final state divided by 2 
        }

		double heuristic1() const{//number of displaced guys from final state divided by 2
			return (abs(cannibals - 3) + abs(missionaries - 3)) / 2.0;
		}

		double heuristic2() const{//number of displaced guys from final state divided by 2
			if (cannibals == 3 && missionaries == 3){
				return 0;
			}
			else{
				return 1;
			}
		}



        bool operator<(const statePCM &rhs) const{
            return (bestDistance + heuristic()) > (rhs.bestDistance + rhs.heuristic());
        }

        int cannibals,missionaries;//on the final shore
        int bestParent;//position of best parent in the closed list;
        int bestDistance;
        int heuisticValue;
        bool source;//true if boat is at source
};

#endif
