#include <iostream>
#include <vector>
#include <queue>

#include "bdpuzzle8.h"
#include "bdastar.h"

using namespace std;



int main(){
	bdastar<state8puzzle> star;
	state8puzzle temp;
	//int arry[] = {4,1,3,7,0,5,8,2,6}; // 9 -- works
	//int arry[] = {4,1,3,0,7,5,8,2,6}; // 10 --works
	//int arry[] = {4,1,3,7,2,5,8,0,6}; // 8 -- works
	//int arry[] = {0,1,3,4,7,5,8,2,6}; // 11 --works
	int arry[] = { 1, 0, 3, 4, 7, 5, 8, 2, 6 }; // 12 --works

	vector<int> start(arry, arry + sizeof(arry) / sizeof(int));
	int arry1[] = {1,2,3,4,5,6,7,8,0};
	vector<int> end(arry1,arry1+ sizeof(arry1)/sizeof(int));
	
	temp.set(start);
	star.setInitialState(temp);
	
	temp.set(end);
	star.setFinalState(temp);
	
	star.solve();

    return 0;

}
