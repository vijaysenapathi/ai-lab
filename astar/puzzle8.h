#ifndef PUZZLE_H
#define PUZZLE_H

#include "astar.h"
#include <iostream>
#include <cmath>
using namespace std;

class state8puzzle{
    public:
        state8puzzle(){
            bestDistance = -1;
            bestParent = -1;
            heuisticValue = -1;
        }
        state8puzzle(const state8puzzle& t){
            values = t.values;
            bestParent = t.bestParent;
            bestDistance = t.bestDistance;
            heuisticValue = t.heuisticValue;
        }
        ~state8puzzle(){

        }
        void set(vector<int> & vec){
            values = vec;
        }

        void swap(int i, int j){
            int temp;
            temp = values[j];
            values[j] = values[i];
            values[i] = temp;
        }
        bool operator==(const state8puzzle& s){
            if (this->values == s.values){
                return true;
            }
            else{
                return false;
            }
        }

        void printGrid(){
            // cout<<"***********************"<<endl;
            // cout<<"*"<<values[0]<<"*******"<<values[1]<<"*******"<<values[2]<<"*****"<<endl;
            // cout<<"*"<<values[3]<<"*******"<<values[4]<<"*******"<<values[5]<<"*****"<<endl;
            // cout<<"*"<<values[6]<<"*******"<<values[7]<<"*******"<<values[8]<<"*****"<<endl;
            // cout<<"***********************"<<endl;
            cout<<values[0]<<" "<<values[1]<<" "<<values[2]<<endl;
            cout<<values[3]<<" "<<values[4]<<" "<<values[5]<<endl;
            cout<<values[6]<<" "<<values[7]<<" "<<values[8]<<endl;
            cout<<"-----"<<endl;
        }

        void print(){
            for(int i=0;i<values.size();i++){
                cout<<values[i]<<"  ";
            }
            cout<<endl;
        }

        vector<state8puzzle> &nextstate(){
            state8puzzle newpuzzle;
            state8puzzle state = *this;
            vector<state8puzzle> *result = new vector<state8puzzle>;

            //0 1 2
            //3 4 5
            //6 7 8
            //int temp;
            if (state.values[0] == 0){
                newpuzzle = state;
                newpuzzle.swap(0, 1);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(0, 3);
                result->push_back(newpuzzle);

            }
            else if (state.values[1] == 0){
                newpuzzle = state;
                newpuzzle.swap(0, 1);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(2, 1);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(4, 1);
                result->push_back(newpuzzle);
            }
            else if (state.values[2] == 0){
                newpuzzle = state;
                newpuzzle.swap(2, 1);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(2, 5);
                result->push_back(newpuzzle);
            }
            else if (state.values[3] == 0){
                newpuzzle = state;
                newpuzzle.swap(0, 3);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(3, 4);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(3, 6);
                result->push_back(newpuzzle);
            }
            else if (state.values[4] == 0){
                newpuzzle = state;
                newpuzzle.swap(4, 1);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(4, 3);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(4, 5);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(4, 7);
                result->push_back(newpuzzle);
            }
            else if (state.values[5] == 0){
                newpuzzle = state;
                newpuzzle.swap(5, 2);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(5, 4);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(5, 8);
                result->push_back(newpuzzle);
            }
            else if (state.values[6] == 0){
                newpuzzle = state;
                newpuzzle.swap(6, 3);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(6, 7);
                result->push_back(newpuzzle);
            }
            else if (state.values[7] == 0){
                newpuzzle = state;
                newpuzzle.swap(7, 4);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(7, 6);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(7, 8);
                result->push_back(newpuzzle);
            }
            else if (state.values[8] == 0){
                newpuzzle = state;
                newpuzzle.swap(8, 7);
                result->push_back(newpuzzle);

                newpuzzle = state;
                newpuzzle.swap(8, 5);
                result->push_back(newpuzzle);
            }
            else{
                cout << "Should not come here" << endl;
            }
            return *result;
        }

        double misplacedTiles() const{
            double sum = 0;
            double temp;

            for(int i=0;i<8;i++){
               temp = (values[i] == i+1)?0:1;
               sum += temp;
            }

            if(values[8] != 0){
               sum+=1;
            }
            return sum;
        }

        double manhattanDistance() const{
            double sum = 0;
            int row,column;
            int expectedRow,expectedColumn;
            int val;
            for(unsigned int i=0;i<values.size();i++){
                row = i / 3;
                column = i % 3;
                val = values[i];


                switch(val){
                    case 0:
                        expectedRow = 2;
                        expectedColumn = 2;
                        break;
                    case 1:
                        expectedRow = 0;
                        expectedColumn = 0;
                        break;
                    case 2:
                        expectedRow = 0;
                        expectedColumn = 1;
                        break;
                    case 3:
                        expectedRow = 0;
                        expectedColumn = 2;
                        break;
                    case 4:
                        expectedRow = 1;
                        expectedColumn = 0;
                        break;
                    case 5:
                        expectedRow = 1;
                        expectedColumn = 1;
                        break;
                    case 6:
                        expectedRow = 1;
                        expectedColumn = 2;
                        break;
                    case 7:
                        expectedRow = 2;
                        expectedColumn = 0;
                        break;
                    case 8:
                        expectedRow = 2;
                        expectedColumn = 1;
                        break;
                    default:
                        //nothing
                        cout<<"Value stored is not anything"<<endl;
                }

                sum += abs(row - expectedRow) + abs(column - expectedColumn);
            }
            return sum;
        }

        double heuristic() const{
            return manhattanDistance();
            //return misplacedTiles();
        }



        bool operator<(const state8puzzle &rhs) const{
            return (bestDistance + heuristic()) > (rhs.bestDistance + rhs.heuristic());
        }

        vector<int> values;
        int bestParent;//position of best parent in the closed list;
        int bestDistance;
        int heuisticValue;
};

#endif
