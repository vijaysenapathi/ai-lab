#include <iostream>
#include <vector>
#include <queue>

#include "puzzle8.h"
#include "astar.h"

using namespace std;



int main(){
	astar<state8puzzle> star;
	state8puzzle temp;

	// int arry[] = { 7, 8, 5, 4, 6, 2, 3, 1, 0 };
	//int arry[] = {0,2,3,8,5,6,4,1,7}; // 20
	//int arry[] = {8,6,7,2,5,4,3,0,1}; // 31
	//int arry[] = {3,2,0,1,5,4,7,8,6}; // 14
	//int arry[] = {0,2,3,5,8,6,1,4,7}; // 16
	//int arry[] = {4,1,3,7,0,5,8,2,6}; // 9
    
	
	//int arry[] = {4,1,3,7,0,5,8,2,6}; // 9 -- works
	//int arry[] = {4,1,3,0,7,5,8,2,6}; // 10 --works
	int arry[] = {4,1,3,7,2,5,8,0,6}; // 8 -- works
	//int arry[] = {0,1,3,4,7,5,8,2,6}; // 11 --works
	//int arry[] = { 1, 0, 3, 4, 7, 5, 8, 2, 6 }; // 12 --works

	vector<int> start(arry, arry + sizeof(arry) / sizeof(int));
	int arry1[] = {1,2,3,4,5,6,7,8,0};
	vector<int> end(arry1,arry1+ sizeof(arry1)/sizeof(int));
	
	temp.set(start);
	star.setInitialState(temp);
	
	temp.set(end);
	star.setFinalState(temp);
	
	star.solve();

    return 0;

}
