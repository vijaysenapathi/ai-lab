#include <iostream>
#include <vector>
#include <cmath>
#include <time.h>
#include <cstdlib>
#include <ostream>
using namespace std;

class perceptron
{
	public:
		perceptron(int,bool,double learning_rate_,double momentum_factor_);
		perceptron(const perceptron&);
		~perceptron();
		double learning_rate;
		double default_weight;
		double momentum_factor;
		int num_inputs;//number of inputs
		vector<double> weight;//weights for each input
		vector<double> prev_del_weight;// previous update in weights useful for momentum 
		bool isThresold;

		double compute(vector<double> inputs); // takes inputs and computes the output 
		double get_output(vector<double> inputs);
		double latest_output;//the latest output
		double latest_delta;//the latest delta
		//set weights
		void set_weights(istream &f);
		// update weights
		void update_weights(vector<double> &wts);
		void print_weights(ostream &f);
};

perceptron::perceptron(int n,bool thres,double learning_rate_,double momentum_factor_){
	num_inputs = n;
	learning_rate = learning_rate_;
	momentum_factor = momentum_factor_;
	isThresold = thres;
	//default_weight = 0.5;
	for (int i = 0; i < num_inputs; i++){
		default_weight = rand()/(double)RAND_MAX;
		//cout << default_weight <<  " ";
		weight.push_back(default_weight);
		prev_del_weight.push_back(0);
	}
	//cout << endl;
	latest_output = thres;
}

perceptron::perceptron(const perceptron& othr){
	num_inputs = othr.num_inputs;
	learning_rate = othr.learning_rate;
	momentum_factor = othr.momentum_factor;
	latest_output = othr.latest_output;
	default_weight = othr.default_weight;
	weight.clear();
	prev_del_weight.clear();
	latest_output = othr.latest_output;
	isThresold = othr.isThresold;
	for (int i = 0; i < num_inputs; i++){
		weight.push_back(othr.weight[i]);
		prev_del_weight.push_back(othr.prev_del_weight[i]);
	}
}

perceptron::~perceptron(){
}

double perceptron::compute(vector<double> inputs){
	if (! isThresold) latest_output = get_output(inputs);
	return latest_output;
}

double sigmoid(double val){
	return 1 / ( 1 + exp( - val ) );
}

double perceptron::get_output(vector<double> inputs){
	if (isThresold) return latest_output;
	double sum;
	sum = 0;
	for (int i = 0; i < num_inputs; i++){
		sum += inputs[i] * weight[i];
	}
	return sigmoid(sum);
}

void perceptron::update_weights(vector<double> &wts){
	for (int i = 0; i < num_inputs; i++){
		weight[i] 
			+=
			wts[i]*latest_delta*learning_rate 
			+
			momentum_factor*prev_del_weight[i];
		prev_del_weight[i] = wts[i]*latest_delta*learning_rate;
	}
}

void perceptron::print_weights(ostream &f){
	f << "  " ;
	for (int i = 0; i < num_inputs; i++)
	{
		f << weight[i] << " ";
	}
	f << endl;
}

void perceptron::set_weights(istream &f){
	for (int i = 0; i < num_inputs; i++){
		f >> weight[i];
	}
}
