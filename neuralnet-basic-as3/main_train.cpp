#include "network.h"
#include <fstream>

template <class type>
bool vec_equal(std::vector<type> v1,std::vector<type> v2){
	if (v1.size() != v2.size()) return false;
	for (int i = 0; i < v1.size(); ++i)
	{
		if (abs(v1[i]-v2[i]) > 0.5) return false;
	}
	return true;
};

bool check_all_cases(Network &n,vector< vector<double> > in_out_samples,bool print){
	int pass=0;
	int tot = 0;
	for (int i = 0; i < in_out_samples.size()/2; ++i)
	{
		vector<double> in(in_out_samples[2*i]);
		if (print)
			for (int j = 0; j < in.size(); ++j)
			{
				cout << in[j] << " ";
			}
		in.push_back(1);

		if (print){
			if (vec_equal<double> (n.get_value_(in),in_out_samples[2*i+1])) {
				pass++;
			}
		}else {
			if (vec_equal<double> (n.get_value(in),in_out_samples[2*i+1])) {
				pass++;
			}
		}

		tot++;
	}
	// if (print)
		cout << "Pass/Total:" << pass <<"/" << tot << endl;
	return tot==pass;
}

int main(){
	srand(time(NULL));
		
	// level == layer
	// levels is number of levels of perceptrons
	// level_elems[0] is number of inputs to 0-th level perceptrons (excluding bias)
	// level_elems[i] where i!=0 is number of perceptrons to (i-1)th level perceptrons

	// const int no_of_layers = 2; // for IRIS
	// int raw_data[] = {4,7,2};
	// double error_limit =1.0;

	// const int no_of_layers = 2; // for Tweets
	// int raw_data[] = {453,8,2};
	// double error_limit =6.0;
	
	// const int no_of_layers = 2; // for monk
	// int raw_data[] = {6,10,1};
	// // double error_limit =0.01; // monk-1 or monk-3
	// double error_limit =12; // monk-2
	
	const int no_of_layers = 2; // for Annotated Tweets
	int raw_data[] = {1498,10,2};
	double error_limit =20.0;
	
	// CONFIG
	double learning_rate = 0.3;
	double momentum_factor = 0.4;

	// CONFIG
	int no_of_iterations = 500;
	bool end_after_iterations = true;

	bool end_after_all_pass = false;
	
	bool end_after_error_limit = true; // cur_lim = 0.01
	
	bool enable_testing = true;

	//const char * file_name = "/home/varun/repos/ai-lab/part3/input/5input_palin.txt";
	//const char * file_name = "/home/varun/repos/ai-lab/part3/input/7_segment_output.txt";
	// const char * file_name = "input/5input_palin.txt";
	// const char * file_name = "input/2input_xor.txt";
	const char * file_name = "../nnets_test/input.data";

	vector<int> level_num_elems(raw_data,raw_data+no_of_layers+1);

	Network n(no_of_layers,level_num_elems,learning_rate,momentum_factor);
	ifstream infile;
	infile.open(file_name);

	vector< vector<double> > in_out_samples;
	int no_inputs,no_outputs;
	double temp;
	if (infile.good()){
		infile >> no_inputs;
		infile >> no_outputs;
		//cout << no_inputs << " " << no_outputs << endl;

		while(infile.good()){
			vector<double> in,out;
			for(int i = 0; i <no_inputs; i++) { 
				infile >> temp;
				in.push_back(temp);
			}
			for(int i = 0; i <no_outputs; i++) {
				infile >> temp;
				out.push_back(temp);
			}
			if (infile.eof()) break;
			in_out_samples.push_back(in);
			in_out_samples.push_back(out);
		}
	}
	else{
		std::cout << "Error opening file";
		return 1;
	}

	// cout << in_out_samples.size() << "::\n";
	int iterations=0;
	int convergence = 0;
	while (true) {
		if (end_after_iterations && iterations > no_of_iterations) break;
		for (int i = 0; i < in_out_samples.size(); i+=2)
		{
			n.train(in_out_samples[i],in_out_samples[i+1]);
		}
		iterations++;
		//if (n.layers[no_of_layers-1].error > 0.003 && n.layers[no_of_layers-1].error <= 0.02) 
		// if (iterations%1000 == 0) 
			cout << iterations  << " " << n.layers[no_of_layers-1].error << endl;
		if (end_after_error_limit && n.layers[no_of_layers-1].error < error_limit)
			if (convergence==10) break;
			else convergence++;
		else convergence = 0;
		if (end_after_all_pass && check_all_cases(n,in_out_samples,0)) break;
		n.layers[no_of_layers-1].nullify_error();
	}
	ofstream outfile;
	outfile.open("weights.data");
	outfile << level_num_elems.size() << endl;
	for (int i = 0; i < level_num_elems.size(); ++i)
	{
		outfile << level_num_elems[i] << " ";
	}
	outfile << endl;
	n.print_weights(outfile);
	outfile.close();
	// cout << endl;
	
	if (check_all_cases(n,in_out_samples,0)){
		cout << "All cases passed.\n";
	} else {
		cout << "All cases not passed.\n";
	}

	while(enable_testing){
		vector<double> in,ans;
		cout << "In: " ;
		for (int i = 0; i < level_num_elems[0]; i++)
		{
			double d;
			cin >> d;
			in.push_back(d);
		}
		in.push_back(1);
		ans = n.get_value(in);
		cout << "Out: " ;
		for (int i = 0; i < ans.size(); i++) cout << ans[i] <<" "; cout << endl;
	}
	return 0;
}
