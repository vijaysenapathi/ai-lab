#include "layer.h"

class Network
{
	public:
		// level == layer
		// levels is number of levels of perceptrons
		// level_elems[0] is number of inputs to 0-th level perceptrons
		// level_elems[i] where i!=0 is number of perceptrons to (i-1)th level perceptrons
		Network(int levels,vector<int> level_elems,double learning_rate,double momentum_factor); 
		~Network();

		int n_levels;                   // number of levels
		vector<Layer> layers;           // vector containing all layers

		void train(vector<double> in,vector<double> out); // train the networkn with this specific input and output
		void update_weights();
		void print_weights(ostream &f); //print weights of all perceptrons
		vector<double> get_value(vector<double> in);
		vector<double> get_value_(vector<double> in);
		void set_weights(istream& f);
	private:
		vector<double> current_target;
};

Network::Network(int levels,vector<int> level_elems,double learning_rate,double momentum_factor){
	n_levels = levels;

	// layer(inputs,num_perceptrons)
	for (int i = 0; i < levels; ++i)
	{
		Layer l(level_elems[i]+1,level_elems[i+1],learning_rate,momentum_factor);
		layers.push_back(l);
	}
}

Network::~Network(){
}

vector<double> Network::get_value_(vector<double> in){
	// cout << "-----------------------DEBUG--------------------------\n";
	for(int i = 0; i < n_levels-1; i++)
	{
		in = layers[i].get_output(in);
		// cout << "Output at level "<<(i+1)<<":\n";
		// for (int k = 0; k < in.size(); k++)
		// {
		// 	cout << in[k] << " ";
		// }
		// cout << endl;

		for (int k = 0; k < in.size(); k++)
		{
			cout << ((in[k]>0.5)?1:0) << " ";
		}
		cout << endl;
		in.push_back(1);
	}
	in = layers[n_levels-1].get_output(in);
	// cout << "------------------------------------------------------\n";
	return in;
}

vector<double> Network::get_value(vector<double> in){
	for(int i = 0; i < n_levels-1; i++)
	{
		in = layers[i].get_output(in);
		in.push_back(1);
	}
	in = layers[n_levels-1].get_output(in);
	return in;
}

void Network::train(vector<double> in,vector<double> out){
	in.push_back(1); //this is for the bias input. 

	if(in.size() != layers[0].num_inputs) {
		cerr << "Training Input size doesnot match!"  << layers[0].num_inputs << " " << in.size() << endl;
		exit(1);
	}
	if(out.size() != layers[n_levels-1].num_perceptrons) {
		cerr << "Training Output size doesnot match!" << endl;
		exit(1);
	}

	current_target.reserve(out.size());
	current_target = out;
	vector<double> temp_in = in;
	for(int i = 0; i < n_levels; i++)
	{
		temp_in = layers[i].compute_output(temp_in);
		temp_in.push_back(1);
	}

	//calculate delta for outermost layer
	vector<double> top_layer_del = layers[n_levels-1].calculate_delta(current_target,true); // 1 means outermost layer

	//calculate delta for hidden layer
	//uses a vector from the next layer to calculate the required value;
	for (int i = n_levels-2; i >= 0; i--)
	{
		layers[i].calculate_delta(layers[i+1].get_next_delta_ws(),false);
	}

	// first updating weights for 0th layer.
	layers[0].update_weights(in);
	for (int i = 1; i < n_levels; i++)
	{
		// updating weights based on prev layer outputs.
		vector<double> tempv = layers[i-1].get_latest_outputs();
		tempv.push_back(1);
		layers[i].update_weights(tempv);
	}
}

void Network::print_weights(ostream& f){
	for (int i = 0; i < n_levels; i++)
	{
		//f << "Layer " << i << ":\n";
		layers[i].print_weights(f);
	}
}

void Network::set_weights(istream& f){
	for (int i = 0; i < n_levels; i++)
	{
		//f << "Layer " << i << ":\n";
		layers[i].set_weights(f);
	}
}
