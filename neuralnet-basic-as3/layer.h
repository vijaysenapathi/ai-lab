#include "perceptron.h"

class Layer
{
	public:
		Layer(int n_i,int n_p,double learning_rate,double momentum_factor);
		Layer(const Layer& othr);
		int num_inputs; //Number of inputs for a perceptron in this layer
		int num_perceptrons; //Number of perceptrons in this layer excluding the bias
		vector<perceptron> perceptrons; // The perceptron objects

		//Computes the output of each perceptron on the given input
		vector<double> compute_output(vector<double> inputs);
		// compute_output does modify the latest_output variable in perceptron
		// get_output doesn't modify the latest_output variable in perceptron
		vector<double> get_output(vector<double> inputs);
		vector<double> get_latest_outputs();
		void print_latest_outputs();
		vector<double> get_next_delta_ws();
		vector<double> calculate_delta(vector<double> ,bool is_outer_layer);
		void update_weights(vector<double> previous_out);
		void print_weights(ostream &f);
		void set_weights(istream &f);

		void nullify_error();
		double error;
};

Layer::Layer(int n_i,int n_p,double learning_rate,double momentum_factor){
	num_inputs = n_i;
	num_perceptrons = n_p;
	for (int i = 0; i < n_p; ++i)
	{
		perceptron s(n_i,0,learning_rate,momentum_factor);
		perceptrons.push_back(s);
	}
	perceptron s(n_i,1,learning_rate,momentum_factor);
	perceptrons.push_back(s);

	error = 0;
}

Layer::Layer(const Layer& othr){
	num_inputs = othr.num_inputs;
	num_perceptrons = othr.num_perceptrons;
	perceptrons = othr.perceptrons;

	error = othr.error;
}

// get_output doesnt modify the latest_output variable in perceptron
vector<double> Layer::get_output(vector<double> inputs){
	vector<double> out;
	if (inputs.size()!=num_inputs)
		cerr << "Layer::get_output Number of inputs does not match\n"; 
	for (int i = 0; i < num_perceptrons; i++)
	{
		out.push_back(perceptrons[i].get_output(inputs));
	}
	return out;
}

// compute_output does modify the latest_output variable in perceptron
vector<double> Layer::compute_output(vector<double> inputs){
	vector<double> out;
	if (inputs.size()!=num_inputs)
		cerr << "layer.h ::Layer::compute_output Number of inputs does not match\n";
	for (int i = 0; i < num_perceptrons; i++)
	{
		out.push_back(perceptrons[i].compute(inputs));
	}
	return out;
}

vector<double> Layer::get_latest_outputs(){
	vector<double> out;
	for (int i = 0; i < num_perceptrons; i++)
	{
		out.push_back(perceptrons[i].latest_output);
	}
	return out;
}

void Layer::print_latest_outputs(){
	for (int i = 0; i < num_perceptrons; i++)
	{
		cout << perceptrons[i].latest_output << " ";
	}
	cout << endl;
}

vector<double> Layer::get_next_delta_ws(){
	vector<double> ans;
	for (int j = 0; j < num_inputs; j++)
	{
		double cur=0;
		for (int k = 0; k < num_perceptrons; k++)
		{
			cur+=perceptrons[k].latest_delta*perceptrons[k].weight[j];
		}
		ans.push_back(cur);
	}
	return ans;
}

void Layer::nullify_error(){
	error = 0;
}

vector<double> Layer::calculate_delta(vector<double> output,bool is_outer_layer){
	vector<double> cur_delta;
	if (is_outer_layer) {
		for (int i = 0; i < num_perceptrons; i++)
		{
			double latest_output = perceptrons[i].latest_output;
			perceptrons[i].latest_delta = (output[i]-latest_output)*(latest_output)*(1-latest_output);
			cur_delta.push_back(perceptrons[i].latest_delta);
			error+=(output[i]-latest_output)*(output[i]-latest_output);
			// cout << "error:" << error << ":" << latest_output <<  ":" << output[i] << endl;
		}
	} else {
		for (int i = 0; i < num_perceptrons; i++)
		{
			double latest_output = perceptrons[i].latest_output;
			double next_layer_factor = output[i];
			perceptrons[i].latest_delta = next_layer_factor*(latest_output)*(1-latest_output);
			cur_delta.push_back(perceptrons[i].latest_delta);
		}
	}
	return cur_delta;
}

void Layer::update_weights(vector<double> previous_out){
	for (int i = 0; i < num_perceptrons; i++)
	{
		perceptrons[i].update_weights(previous_out);
	}
}

void Layer::print_weights(ostream &f){
	for (int i = 0; i < num_perceptrons; i++)
	{
		// f << "  Perceptron " << i << " :";
		perceptrons[i].print_weights(f);
	}
}

void Layer::set_weights(istream &f){
	for (int i = 0; i < num_perceptrons; i++)
	{
		// f << "  Perceptron " << i << " :";
		perceptrons[i].set_weights(f);
	}
}