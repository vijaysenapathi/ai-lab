#include "network.h"
#include <fstream>

template <class type>
bool vec_equal(std::vector<type> v1,std::vector<type> v2){
	if (v1.size() != v2.size()) return false;
	for (int i = 0; i < v1.size(); ++i)
	{
		if (abs(v1[i]-v2[i]) > 0.5) return false;
	}
	return true;
};

bool check_all_cases(Network &n,vector< vector<double> > in_out_samples,bool print){
	int pass=0;
	int tot = 0;
	for (int i = 0; i < in_out_samples.size()/2; ++i)
	{
		vector<double> in(in_out_samples[2*i]);
		if (print)
			for (int j = 0; j < in.size(); ++j)
			{
				cout << in[j] << " ";
			}
		in.push_back(1);

		if (print){
			if (vec_equal<double> (n.get_value_(in),in_out_samples[2*i+1])) {
				pass++;
			}
		}else {
			if (vec_equal<double> (n.get_value(in),in_out_samples[2*i+1])) {
				pass++;
			}
		}

		tot++;
	}
	// if (print)
		cout << "Pass/Total:" << pass <<"/" << tot << endl;
	return tot==pass;
}

int main(){
	srand(time(NULL));
		
	// level == layer
	// levels is number of levels of perceptrons
	// level_elems[0] is number of inputs to 0-th level perceptrons (excluding bias)
	// level_elems[i] where i!=0 is number of perceptrons to (i-1)th level perceptrons
	int no_of_layers;
	vector<int> level_num_elems;
	// CONFIG
	double learning_rate = 0.99;
	double momentum_factor = 1.00; 
	
	int temp; //temporary variable
	int no_inputs,no_outputs;
	
	ifstream infile;
	infile.open("weights.data");
	infile >> no_of_layers;
	//cout<< no_of_layers<<endl;
	infile >> no_inputs;
	level_num_elems.push_back(no_inputs);
	for (int i = 1; i < no_of_layers-1; i++)
	{
		infile >> temp;
		level_num_elems.push_back(temp);
	}
	infile >> no_outputs;
	level_num_elems.push_back(no_outputs);

	Network n(no_of_layers-1,level_num_elems,learning_rate,momentum_factor);
	n.set_weights(infile);
	infile.close();

	infile.open("../nnets_test/check.data");

	vector< vector<double> > in_out_samples;
	double dtemp;
	if (infile.good()){

		while(infile.good()){
			vector<double> in,out;
			for(int i = 0; i <no_inputs; i++) { 
				infile >> dtemp;
				in.push_back(dtemp);
			}
			for(int i = 0; i <no_outputs; i++) {
				infile >> dtemp;
				out.push_back(dtemp);
			}
			if (infile.eof()) break;
			in_out_samples.push_back(in);
			in_out_samples.push_back(out);
		}
	}
	else{
		std::cout << "Error opening file";
		return 1;
	}

	if (check_all_cases(n,in_out_samples,1)){
		cout << "All cases passed.\n";
	} else {
		cout << "All cases not passed.\n";
	}



	return 0;
}
