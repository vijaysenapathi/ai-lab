#ifndef WFF_H
#define WFF_H

#include <iostream>
#include <string>
#include <vector>
using namespace std;

class wff{

public:
	bool is_leaf;
	wff* ltree;
	wff* rtree;
	string val;
	wff(string v);
	wff(wff* l,wff *r);
	~wff();
	bool operator==(const wff &);
	void print();
};

class solver{
	wff* formula;
	vector<wff*> hyp;
public:
	bool symantic_check();
	wff* get_axiom_1(wff* a,wff* b);
	wff* get_axiom_2(wff* a,wff* b,wff* c);
	wff* get_axiom_3(wff* a);
	wff* get_axiom_input();
	solver(){};
	bool solve(wff*);
};

#endif

