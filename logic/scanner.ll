%filenames="scanner"
%lex-source="scanner.cc"


%%

"AND" {
  //cerr << "TOK AND\n";
  return Parser::AND;
}

"OR" {
  //cerr << "TOK OR\n";
  return Parser::OR; 
}

"NOT" {
  //cerr << "TOK NOT\n";
  return Parser::NOT; 
}

[[:alpha:]_][[:alpha:][:digit:]_]* {
  ParserBase::STYPE__ * val = getSval();
  val->string_value = new std::string(matched());

  //cerr << "TOK NAME\n";
  return Parser::NAME;
}

[[:digit:]]+  { 
  ParserBase::STYPE__ * val = getSval();
  val->string_value = new std::string(matched());

  //cerr << "TOK NAME\n";
  return Parser::NAME;
//        ParserBase::STYPE__ * val = getSval();
//        val->integer_value = atoi(matched().c_str());

//        return Parser::INTEGER_NUMBER;
}

[-)(>:,] {
  return matched()[0]; 
}

[ \t\n]		{ }

.	{ 
  string error_message;
  error_message =  "Illegal character `" + matched();
  error_message += "' on line " + lineNr();
  //cerr << error_message;	
  exit(1);
}

%%
