%scanner ../scanner.h
%scanner-token-function d_scanner.lex()
%filenames parser
%parsefun-source parser.cc

%token NAME NOT AND OR
%union 
{
  std::string * string_value;
  wff * wff_ptr;
  std::vector<wff*>* wff_ptr_list;
  int integer_value;
  // pair<int,vector<wff*>*> axiom_type;
  vector<vector<wff*>*>* axiom_list_type;
};

%type <wff_ptr> WFF  
%type <wff_ptr> unit
%type <wff_ptr_list> param_list
%type <wff_ptr_list> axiom
%type <axiom_list_type> axiom_list_
%type <string_value> NAME
%start Start

%%

Start:
WFF ':' axiom_list_
{
  cur_formula = $1;
  axiom_list = $3;  
}
|
WFF
{
  cur_formula = $1;
}
;

axiom_list_:
  axiom
  {
    $$ = new vector<vector<wff*>*>;
    $$->push_back($1);
  }
|
  axiom_list_ ',' axiom
  {
    $$ = $1;
    $$->push_back($3);
  }
;

axiom:
WFF ':' param_list
{
  $$ = $3;
  $$->push_back($1);
};

param_list:
  WFF
  {
    $$ = new vector<wff*>;
    $$->push_back($1);
  }
|
  param_list ':' WFF
  {
    $$ = $1;
    $$->push_back($3);
  }
;

WFF : 
  unit
|
  unit '-' '>' unit
  {
    $$ = new wff($1,$4);
  }
|
  NOT unit
  {
    $$ = new wff($2,new wff(special_symbol));
  }
|
  unit OR unit
  {
    $$ = new wff(new wff($1,
                         new wff(special_symbol)),
                 $3);
  }
|
  unit AND unit
  {
    $$ = new wff(new wff($1,
                         new wff($3,
                                 new wff(special_symbol)
                                )
                        ),
                 new wff(special_symbol));
  }
;

unit :
  '(' WFF ')' 
  {
    $$=$2;
  }
|
  NAME
  {
    $$ = new wff(*$1);
    //cout << (*$1) << " ";
  }
; 