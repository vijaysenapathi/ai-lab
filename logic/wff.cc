#include "wff.h"
#include <algorithm>
#include <set>

string special_symbol = "F";
wff *cur_formula;
vector<vector<wff*>*>* axiom_list;

wff::wff(string v){
	is_leaf = true;
	val = v;
}

wff::wff(wff* l,wff *r){
	is_leaf = false;
	ltree = l;
	rtree = r;
}
wff::~wff(){
	if (!is_leaf){
		delete ltree;
		delete rtree;
	}
}

bool wff::operator==(const wff &s){
	if (is_leaf == s.is_leaf){
		if (is_leaf) return val == s.val;
		else return *ltree==*s.ltree && *rtree==*s.rtree;
	}
	return false;
}

void wff::print(){
	if (is_leaf)
		cout << val ;
	else{
		cout << "(";
		ltree->print();
		cout << " -> ";
		rtree->print();
		cout << ")";
	}
}

wff* solver::get_axiom_1(wff* a,wff* b){
	wff* cur = new wff(b,a);
	return new wff(a,cur);
}

wff* solver::get_axiom_2(wff* a,wff* b,wff* c){
	wff* bc = new wff(b,c);
	wff* ab = new wff(a,b);
	wff* ac = new wff(a,c);
	return new wff(new wff(a,bc),new wff(ab,ac));
}

wff* solver::get_axiom_3(wff* a){
	wff* cur = new wff(a,new wff("F"));
	return new wff(new wff(cur,new wff("F")),a);
}

bool solver::symantic_check(){
}

vector<wff*> get_modes_ponens(vector<wff*> &hyp){
	vector<wff*> mp;
	for(int i =0; i < hyp.size();i++){
		for(int j=0; j <hyp.size();j++) {
			if (i!=j){
				if (hyp[j]->is_leaf) continue;
				if (*hyp[i] == *(hyp[j]->ltree)) {
					mp.push_back(hyp[j]->rtree);
				}
			}
		}
	}
	set<wff*> s( mp.begin(), mp.end() );
	mp.assign( s.begin(), s.end() );
	return mp;
}

class Comparator
{
  public:
    int operator() ( wff* p1, wff *p2)
    {
    	if (!p1) return false;
    	if (!p2) return false;
        return !(*p1 == *p2);
    }
};

typedef wff* wp;

bool is_solved(vector<wp> hyp,wp res){
	for (int i = 0; i < hyp.size(); ++i)
	{
		if (*hyp[i] == *res) return true;
	}
	return false;
}

// wp read_axiom_input(){
// 	int axiom_id ;
// 	cout << "Which axiom should be used?[1/2/3]:" << endl;
// 	freopen("axiom_id", "r", stdin);
// 	cin >> axiom_id;
// 	if (axiom_id == 1) {
// 		cout << "A1 : (A->(B->A))\nSet A in axiom_in_A and B in axiom_in_B\n";
// 	} else if (axiom_id == 2){
// 		cout << "A2 : (A->(B->C))->((A->B)->(A->C))\nSet A in axiom_in_A and B in axiom_in_B and C in axiom_in_C\n";
// 	} else {
// 		cout << "A3 : ((A->F)->F)->A\nSet A in axiom_in_A\n";
// 	}
// }
int index = 0;
wp solver::get_axiom_input(){
	if (axiom_list == NULL || index==axiom_list->size()){
		return NULL;
	}else{
		vector<wff*> cur = *((*axiom_list)[index]);
		index++;
		if (cur[cur.size()-1] == new wff("1")){
			return get_axiom_1(cur[0],cur[1]);
		} else if (cur[cur.size()-1] == new wff("2")){
			return get_axiom_2(cur[0],cur[1],cur[2]);
		} else {
			return get_axiom_3(cur[0]);
		}
	}
}

bool solver::solve(wff* cur){
	cout << "In: " ;
	cur->print();
	cout << endl;

	formula = cur;
	set<wff*,Comparator> s;

	while(! cur->is_leaf) {
		s.insert(cur->ltree);
		cur = cur->rtree;
	}
	hyp.assign( s.begin(), s.end() ); // Applied deduction theorem and pulled out all hyps


	int iterations=0;
	int prev_mp_s = -1;
	// vector<wp> prev_mp;
	vector<wff*> mp,prev_mp;
	for (int i = 0; i < hyp.size(); ++i)
	{
		cout << "Hypothesis: ";hyp[i]->print();cout << endl;
	}
	cout << "To prove: " ;cur->print();cout << endl;
	do {
		iterations++; 
		if (iterations>5)break;
		cout << "-------------\nIteration No:" << iterations << endl;
		mp = get_modes_ponens(hyp);
		for (int i = 0; i < mp.size(); ++i)
		{
			s.insert(mp[i]);
		}
		
		if (prev_mp_s == mp.size()){
			wp cur_ax = get_axiom_input();
			if (cur_ax){
				s.insert(cur_ax);
				cout << "Axiom inserted into the hypothesis\n";
				cur_ax->print() ; cout << endl;
			}
			else
				break;
		}
		prev_mp_s = mp.size();

		hyp.assign( s.begin(), s.end() );
		for (int i = 0; i < mp.size(); ++i)
		{
			cout << "MP: ";mp[i]->print();cout << endl;
		}
		// for (int i = 0; i < hyp.size(); ++i)
		// {
		// 	cout << "Lines: ";hyp[i]->print();cout << endl;
		// }
	}while(!is_solved(hyp,cur));

	if (is_solved(hyp,cur)) cout << "Proved.\n" ;
	else cout << "Not Proved.Human help needed,please add axiom input in file.\n" ;

	return true;
}


// bool solver::solve(wff* cur){
// 	cout << "In: " ;
// 	cur->print();
// 	cout << endl;
// 	vector<wff*> hyp;

// 	formula = cur;
// 	while(! cur->is_leaf) {
// 		hyp.push_back(cur->ltree);
// 		cur = cur->rtree;
// 	}
// 	vector<wff*> mp = get_modes_ponens(hyp);
// 	vector<wff*>::iterator it;
// 	cout << hyp.size() << endl;
// 	cout << mp.size() << endl;
// 	it = set_union(hyp.begin(),hyp.end(),mp.begin(),mp.end(),hyp.begin());
// 	// hyp.insert(hyp.end(), mp.begin(), mp.end());
// 	cout << hyp.size() << endl;
// 	cout << mp.size() << endl;
// 	// hyp.resize(it-hyp.begin());
// 	for (int i = 0; i < hyp.size(); ++i)
// 	{
// 		cout << "Lines: ";hyp[i]->print();cout << endl;
// 	}
// 	cout << "To prove: " ;cur->print();cout << endl;

// 	return true;
// }