#include "wff.h"
#include "parser.h"

extern wff *cur_formula;
int main()
{
	// freopen("in", "r", stdin);
	Parser simcalc("in");
	if (simcalc.parse()) {
		cerr << "Parsing failed\n";
		return 1;
	}
	solver *s = new solver();
	s->solve(cur_formula);
}