and(0,0,0).
and(0,1,0).
and(1,0,0).
and(1,1,1).

or(0,0,0).
or(0,1,1).
or(1,0,1).
or(1,1,1).

not(0,1).
not(1,0).

xor(X,Y,Z) :- not(X,NX), not(Y,NY), and(X,NY,Z1),and(NX,Y,Z2),or(Z1,Z2,Z).

halfadder(In1,In2,Sum,Carry) :- 
	xor(In1,In2,Sum),
	and(In1,In2,Carry).

fulladder(A,B,C,Sum,Carry) :- 
	xor(A,B,A_B),
	xor(A_B,C,Sum),
	and(A,B,AB),
	and(C,A_B,CAB),
	or(AB,CAB,Carry).
