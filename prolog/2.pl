set1(1).
set0(0).

%:- dynamic signal/2.

signal(a,0).
signal(b,1).
signal(c,1).
signal(d,1).
signal(e,0).

signal(X,Y) :-
	out(Gate,X),type(Gate,and),findall(INX,in(INX,Gate),Z),
	member(INY,Z),signal(INY,0),!,set0(Y).
signal(X,Y) :-
	out(Gate,X),type(Gate,and),set1(Y).

signal(X,Y) :-
	out(Gate,X),type(Gate,or),findall(INX,in(INX,Gate),Z),
	member(INY,Z),signal(INY,1),!,set1(Y).
signal(X,Y) :-
	out(Gate,X),type(Gate,or),set0(Y).

%5 input majority

type(gate1,and).
type(gate2,and).
type(gate3,and).
type(gate4,and).
type(gate5,and).
type(gate6,and).
type(gate7,and).
type(gate8,and).
type(gate9,and).
type(gate10,and).
type(gateFinal,or).

in(o1,gateFinal).
in(o2,gateFinal).
in(o3,gateFinal).
in(o4,gateFinal).
in(o5,gateFinal).
in(o6,gateFinal).
in(o7,gateFinal).
in(o8,gateFinal).
in(o9,gateFinal).
in(o10,gateFinal).

in(a,gate1).
in(b,gate1).
in(c,gate1).

in(a,gate2).
in(b,gate2).
in(d,gate2).

in(a,gate3).
in(b,gate3).
in(e,gate3).

in(a,gate4).
in(c,gate4).
in(d,gate4).

in(a,gate5).
in(c,gate5).
in(e,gate5).

in(a,gate6).
in(d,gate6).
in(e,gate6).

in(b,gate7).
in(c,gate7).
in(d,gate7).

in(b,gate8).
in(c,gate8).
in(e,gate8).

in(b,gate9).
in(d,gate9).
in(e,gate9).

in(c,gate10).
in(d,gate10).
in(e,gate10).

out(gate1,o1).
out(gate2,o2).
out(gate3,o3).
out(gate4,o4).
out(gate5,o5).
out(gate6,o6).
out(gate7,o7).
out(gate8,o8).
out(gate9,o9).
out(gate10,o10).
out(gateFinal,major).
