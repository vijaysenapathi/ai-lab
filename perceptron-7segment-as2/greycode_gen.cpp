#include <iostream>
using namespace std;

void print_gc(int n,string prev){
  if (n==0) {
    string sp="";
    for (int i = 0; i < prev.size(); i++)
    {
      if(prev[i]!=' ') sp += prev[i];
    }
    cout << prev+" 0" << endl;
  }
  else {
    print_gc(n-1,prev + " 1");
    print_gc(n-1,prev + " 0");
  }
}

int main(){
  print_gc(7,"");
}
